import "./App.css";

import Context from "./Paper/Context";
import RoterPages from "./Paper/Pages"

// blog project
function App() {  return (
     <div className="App">
       <div className="font-face-gm">
      <Context>
        <RoterPages/> 
      </Context>
    </div> 
  </div> 
  );
}

export default App;
